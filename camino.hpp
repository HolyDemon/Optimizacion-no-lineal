#pragma once
#include "camino.cpp"

using namespace std;

namespace camino{

vector<float> rand_direction_unitario(const int & n);

vector<float> aleatorio(const int & n, vector<float> & x, float & lambda, const float & epsilon, const int & max_iteraciones);

}
