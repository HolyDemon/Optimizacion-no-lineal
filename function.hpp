#include <cmath>
#pragma once

using namespace std;
//distance es parametro
//punto es lo que quieres optimizar
// pow(pow(sensor[i][0] - punto[0] ,2) + pow(sensor[i][1] - punto[1] ,2) + pow(sensor[i][2]-punto[2] ,2) - pow(distancia,2) , 2)
struct Function{
	float operator() (const vector<float> & x){
		//return pow(1.5 - x[0]*(1-x[1]),2) + pow(2.25-x[0]*(1-x[1]*x[1]),2) + pow(2.625 - x[0]*(1-pow(x[1],3)),2);
		return 
	}
	template<typename vector_n>
	float operator() (const vector_n & x){
		//return pow(100*((x(1) - (pow(pow(x(0),2),2))) + (1 - x(0))),2);
		//return pow(1.5 - x(0)*(1-x(1)),2) + pow(2.25-x(0)*(1-x(1)*x(1)),2) + pow(2.625 - x(0)*(1-pow(x(1),3)),2);
		return 0;
	}
	float operator() (const float & x){
		return x;
	}
};

struct dFunction{
	vector<float> operator() (const vector<float> & x){
		vector<float> answer(2);
		answer[0] = x[0];
		answer[1] = x[1];
		return answer;
	}
	template<typename vector_n>
	vector_n operator() (const vector_n & x){
		vector_n answer;
		answer(0) = 4*x(0)*(x(0)*x(0) + x(1) - 11) + 2*(x(0) + x(1)*x(1) - 7);
		answer(1) = 2*(x(0)*x(0) + x(1) - 11) + 4*x(1)*(x(0) + x(1)*x(1) - 7);
		return answer;
	}
	
	float operator() (const float & x){
		return x;
	}
};

struct ddFunction{
	float operator() (const float & x){
		return x;
	}
	template<class matrix_nn, typename vector_n>
	matrix_nn operator() (const vector_n & x){
		matrix_nn answer;
// 		answer(0,0) = -400*x(1) + 1200*(pow(x(0),2)) + 2;
// 		answer(0,1) = -400*x(0);
// 		answer(1,0) = -400*x(0);
// 		answer(1,1) = 200;
		answer(0,0) = 4*(x(0)*x(0) + x(1) - 11) + 8*x(0)*x(0) + 2;
		answer(0,1) = 4*x(0) + 4*x(1);
		answer(1,0) = 4*x(0) + 4*x(1);
		answer(1,1) = 2 + 4*(x(0) + x(1)*x(1) - 7) + 8*x(1)*x(1);
		return answer;
	}
	
};
