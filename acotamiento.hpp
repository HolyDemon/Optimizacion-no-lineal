#pragma once
#include "acotamiento.cpp"
#include "function.hpp"

using interval = vector<float>;

namespace acotar{

interval busqueda_exhaustiva(const interval & intervalo , const int & division);
interval acotacion_1(const float & x0, const float & delta);

}
