#pragma once
#include "aproximacion.cpp"
#include <vector>

using namespace std;
using interval = vector<float>;

namespace aprx {

float min_cuadr(const float & x1, const float & x2, const float & x3);
float min_cubic(const float & x1, const float & x2);
float aprox_cuadratica(const interval & intervalo);
float cuadr_sucesivas(float & x1,  const float & delta, const float & eps);
float biseccion(const interval & intervalo, const float & eps);
float secante(const interval & intervalo, const float & eps);
float aprox_cubica(float & x, float & delta, const float & eps1, const float & eps2);

}
