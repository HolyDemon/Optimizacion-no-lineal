#include <iostream>
#include <cstdlib>
#include <ctime>
#include <cmath>
#include <vector>
#include <eigen3/Eigen/Dense>
#include "function.hpp"

using namespace std;
using namespace Eigen;

Function f;
dFunction df;
ddFunction ddf;

template<typename T>
bool independent(T A){
	T temp = A.transpose();
	A = temp;
	float det = A.determinant();
	if (det != 0){
		return true;
	}
	return false;
}

template<typename vector_n, int n>
vector_n marquad(vector_n & x0, float lambda, const float & epsilon, const int & m){
	int k = 0;
	Matrix<float,n,n> I = Matrix<float,n,n>::Identity();
	vector_n xk = x0;
	float fxk = f(xk);
	vector_n gradient = df(xk);
	
	while ((gradient.squaredNorm() > epsilon*epsilon) && (k < m)){
		while (true){
			Matrix<float,n,n> Hessiana = ddf.operator()<Matrix<float,n,n>,vector_n>(xk);
			Matrix<float,n,n> A = Hessiana + lambda*I;
			if (!(independent(A))){
				cout << "matrix es singular" << endl;
				return x0;
 			}
			Matrix<float,n,n> A_invertido = A.inverse();
			vector_n dk = - 1.0*gradient*A_invertido;
			vector_n new_xk = xk+dk;
			float new_fxk = f(new_xk);
			if (new_fxk >= fxk){
				lambda *= 2;
			}
			else{
				lambda *= 0.5;
				k+=1;
				xk = new_xk;
				fxk = new_fxk;
				gradient = df(xk);
				break;
			}
		}
	}
	return xk;
}

int main(){
	Matrix<float,1,2> mat;
	mat << 0.0f,0.0f;
	Matrix<float,1,2> answer = marquad<Matrix<float,1,2>,2>(mat,0.001f,0.001f,1000);
	cout << "answers" << endl;
	cout << answer(0) << " " << answer(1) << endl;
	Matrix<float,1,2> deriv = df(answer);
	cout << deriv(0) << " " << deriv(1) << endl;
// 	cout << 4*answer(0)*(answer(0)*answer(0) + answer(1) - 11) + 2*(answer(0) + answer(1)*answer(1) - 7) << " ";
// 	cout << 2*(answer(0)*answer(0) + answer(1) - 11) + 4*answer(1)*(answer(0) + answer(1)*answer(1) - 7) << endl;
	return 0;
}
