#include <cstdlib>
#include <ctime>
#include <cmath>
#include "function.hpp"

using namespace std;

namespace camino{

vector<float> rand_direction_unitario(const int & n){
	vector<float> direction(n);
	while (true){
		float R = 0;
		for (int iter = 0; iter < n; iter++){
			direction[iter] = 2*(rand()/RAND_MAX) - 1;
			R = R + direction[iter]*direction[iter];
		}
		R = sqrt(R);
		if (R < 1){
			continue;
		}
		for (int iter = 0; iter < n; iter++){
			direction[iter] = direction[iter]/R;
		}
		return direction;
	}
	return direction;
}

vector<float> aleatorio(const int & n, vector<float> & x, float & lambda, const float & epsilon, const int & max_iteraciones){
	Function f;
	int i = 0; float fx,fxnew;
	vector<float> xnew(n);
	while (lambda > epsilon){
		vector<float> direction = rand_direction_unitario(n);
		for (int iter = 0; iter < n; iter++){
			xnew[iter] = x[iter]+direction[iter]*lambda;
		}
		fx = f(x); fxnew = f(xnew);
		if (fx > fxnew){
			i = 0;
			x = xnew;
		}
		else{
			i++;
			if (i >= max_iteraciones){
				lambda = lambda/2;
			}
		}
	}
	return x;
}

}
