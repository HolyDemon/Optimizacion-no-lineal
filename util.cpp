#include <iostream>
#include "function.hpp"
#include <vector>
#include <algorithm>

using namespace std;

void ordenar(float & x1, float & x2, float & x3){
  float a1,a2,a3;
  a1 = min(x1,min(x2,x3));
  a3 = max(x1,max(x2,x3));
  if ((x1 > a1) && (x1 < a3)){
    a2 = x1;
  }
  else if ((x2 > a1) && (x2 < a3)){
    a2 = x2;
  }
  else{
    a2 = x3;
  }
  x1 = a1;
  x2 = a2;
  x3 = a3;
}

void top_tres(float & x1, float & x2, float & x3, float & x4){
  float f1 = f(x1);
  float f2 = f(x2);
  float f3 = f(x3);
  float f4 = f(x4);
  float maxf = max(max(f1,f2),max(f3,f4));
  if (f2 == maxf){
    float temp = x2;
    x2 = x1;
    x1 = temp;
  }
  else if (f3 == maxf){
    float temp = x3;
    x3 = x1;
    x1 = temp;
  }
  else if (f4 == maxf){
    float temp = x4;
    x4 = x1;
    x1 = temp;
  }
}
