#include <iostream>
#include <vector>
#include <algorithm>
#include "function.hpp"
#include "acotamiento.hpp"
#include "busqueda.hpp"
#include "raices.hpp"
#include "aproximacion.hpp"


using namespace std;
using namespace acotar;
using namespace busque;
using namespace aprx;
using namespace raices;
using interval = vector<float>;

int main(){
  float eps1,eps2,x,delta;
  cin >> eps1 >> eps2 >> x >> delta;
  float respuesta = aprox_cubica(eps1,eps2,x,delta);
  cout << respuesta << endl;
  cout << f(respuesta) << " " << df(respuesta) << endl;
  return 0;
}
