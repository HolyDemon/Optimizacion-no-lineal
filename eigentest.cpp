#include <iostream>
#include <vector>
#include <algorithm>
#include <eigen3/Eigen/Dense>

using namespace Eigen;
using namespace std;

template<typename T>
bool independent(T A){
	T temp = A.transpose();
	A = temp;
	if (A.determinant() != 0){
		return true;
	}
	return false;
}

int main(){
	Matrix<float,3,3> A;
	A << 1.0f,2.0f,0.0f,1.0f,1.0f,2.0f,0.0f,1.0f,9.0f;
	A(0,0) = 3;
	for (int x = 0; x < 3; x++){
		for (int y = 0; y < 3; y++){
			cout << A(x,y) << " ";
		}
		cout << endl;
	}
	if (independent(A)){
		cout << "hi" << endl;
	}
	cout << A.determinant() << endl;
	
}
