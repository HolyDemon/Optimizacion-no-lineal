#pragma once
#include "powell.cpp"

using namespace std;
using namespace Eigen;

bool independent();

template<typename vector_n>
vector<float> powell(const int & n, vector_n & x0, const float & epsilon);
