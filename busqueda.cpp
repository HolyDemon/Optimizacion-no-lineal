#include "busqueda.hpp"

using namespace std;
static float phi = 0.618034;
namespace busque{

vector<float> calculate_fibonacci(const int & n){
  vector<float> fibonacci_sequence (n+1,0);
  for (int iterator = 0; iterator < n; iterator++){
    if (iterator == 0){
      fibonacci_sequence[iterator+1] = 1;
    }
    else{
      fibonacci_sequence[iterator+1] = fibonacci_sequence[iterator]+fibonacci_sequence[iterator-1];
    }
  }
  return fibonacci_sequence;
}

interval busqueda_fibonacci(const interval & intervalo, const int & n){
  int k = 2;
  float b = intervalo[1], a = intervalo[0];
  float length = b-a;
  vector<float> fibonacci = calculate_fibonacci(n);
  float lk;
  while (k <= n){
    lk = (fibonacci[n-k]/fibonacci[n])*length;
    float x1 = a+lk; float x2 = b-lk;
    if (f(x1) > f(x2)){
      a = x1;
    }
    else{
      b = x2;
    }
    k++;
  }
  interval new_intervalo; new_intervalo.push_back(a); new_intervalo.push_back(b);
  return new_intervalo;
}

interval seccion_dorada(const interval & intervalo, const float & eps){
  float a = intervalo[0]; float b = intervalo[1];
  float length = b-a;
  float lk;
  while (length > eps){
    cout << a << " " << b << endl;
    lk = phi*length;
    float x1 = a+lk; float x2 = b-lk;
    if (f(x1) < f(x2)){
      a = x2;
    }
    else{
      b = x1;
    }
    length = b-a;
  }
  interval new_intervalo; new_intervalo.push_back(a); new_intervalo.push_back(b);
  return new_intervalo;
}

}
