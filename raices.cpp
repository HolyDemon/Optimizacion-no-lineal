#include <iostream>
#include "function.hpp"
#include <cmath>

namespace raices{

float newton_raphson(float & x, const float & eps){
  while (abs(df(x)) > eps){
    x = x-(df(x)/ddf(x));
  }
  return x;
}

float derivada_en_x(const float & x, const float & delta){
  float f1 = f(x+delta);
  float f2 = f(x-delta);
  cout << "primera derivada " << (f1-f2)/(2*delta) << endl;
  return (f1-f2)/(2*delta);
}

float derivada_2_en_x(const float & x, const float & delta){
  float f1 = f(x+delta);
  float f2 = f(x-delta);
  float f3 = f(x);
  cout << "segunda derivada_en_x " << (f1-2*f3+f2)/(delta*delta) << endl;
  return (f1-2*f3+f2)/(delta*delta);
}

// Se tiende a infinito :v
float newton_raphson_aprox(float & x, const float & delta, const float & eps){
  int it = 0;
  while (abs(derivada_en_x(x,delta)) > eps && it != 3){
    it++;
    cout << x << endl;
    x = x-(derivada_en_x(x,delta)/derivada_2_en_x(x,delta));
  }
  return x;
}

}
