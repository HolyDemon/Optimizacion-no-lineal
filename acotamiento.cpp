#include <iostream>
#include "function.hpp"
#include <vector>

using namespace std;
using interval = vector<float>;

Function f;
dFunction df;
ddFunction ddf;

namespace acotar{

interval busqueda_exhaustiva(const interval & intervalo, const int & division){
  float a = intervalo[0];  float b = intervalo[1];  float delta = (b-a)/division;
  float a1 = a;  float a2 = a1+delta;  float a3 = a2+delta;
  while (a3 <= b){
    if ((f(a1) >= f(a2)) and (f(a2) <= f(a3))){
      interval acotamiento; acotamiento.push_back(a1);  acotamiento.push_back(a3);
      return acotamiento;
    }
    a1 = a2;  a2 = a3;  a3 = a3+delta;
  }
  interval no_encontrado;  no_encontrado.push_back(0);
  return no_encontrado;
}

interval acotacion_1(const float & x0, const float & delta){
  int k = 2;
  float xnext = x0+2*delta;
  float xnew = x0+delta;
  float xold = x0;
  while (f(xnext) < f(xnew)){
    k++;
    xold = xnew;
    xnew = xnext;
    xnext = xnext+(1<<k)*delta;
  }
  interval intervalo (2); intervalo[0] = xold; intervalo[1] = xnext;
  return intervalo;
}

interval acotacion_2(const float & x0, float & delta){
  float xold = x0;  float xnew;
  if ((f(xold-delta) >= f(xold)) and (f(xold) >= f(xold+delta))){
    xnew = x0+delta;
  }
  else{
    if (f(x0-delta) <= f(x0) and (f(x0) <= f(x0+delta))){
      delta = -delta;
      xnew = x0+delta;
    }
    else{
      interval intervalo (2); intervalo[0] = x0-delta; intervalo[1] = x0+delta;
      return intervalo;
    }
  }
  int k = 2;
  float xnext = xnew + (1<<k)*delta;
  while (f(xnext) < f(xnew)){
    k++;
    xold = xnew;
    xnew = xnext;
    xnext = xnext+(1<<k)*delta;
  }
  interval intervalo (2); intervalo[0] = xold; intervalo[1] = xnext;
  return intervalo;
}

}
