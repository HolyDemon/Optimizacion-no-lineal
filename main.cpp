#include <iostream>
#include <vector>
#include <algorithm>
#include <eigen3/Eigen/Dense>
#include "function.hpp"
#include "camino.hpp"
#include "powell.hpp"
#include "acotamiento.hpp"
#include "busqueda.hpp"
#include "raices.hpp"
#include "aproximacion.hpp"
using namespace std;
using namespace acotar;
using namespace busque;
using namespace aprx;
using namespace raices;
using interval = vector<float>;

int main (){
	vector<float> x(2); int n; float epsilon;
	cin >> n >> x[0] >> x[1] >> epsilon;
	vector<float> answer = powell(n,x,epsilon);
	for (int iter = 0; iter < answer.size(); iter++){
		cout << answer[iter] << " ";
	}
	cout << endl;
	return 0;
}
