#include <iostream>
#include <cstdlib>
#include <ctime>
#include <cmath>
#include <vector>
#include <eigen3/Eigen/Dense>
#include "function.hpp"

using namespace std;
using namespace Eigen;

Function f;

template<typename vector_n>
float f_direction(vector_n x, vector_n d, const float & w, const float & a, const float & b, const int & n){
	float l = (b - a)*w + a;
	vector<float> x_vector;
	for (int iter = 0; iter < n; iter++){
		x_vector.push_back(x(iter) + l*d(iter));
	}
	return f(x_vector);
}

template<typename vector_n>
float seccion_dorada(vector_n x, vector_n d, const float & a, const float & b, const float & epsilon, const int & n){
	float phi = 0.618;
	float aw = 0, bw = 1, lw = 1;
	int aux = 3;
	float lopt = 0;
	float w1,w2,fw1,fw2;
	while (lw > epsilon/(b-a)){
		if (aux == 1){
			w2 = w1;
			fw2 = fw1;
			w1 = aw + phi*lw;
			fw1 = f_direction(x,d,w1,a,b,n);
		}
		else if (aux == 2){
			w1 = w2;
			fw1 = fw2;
			w2 = bw - phi*lw;
			fw2 = f_direction(x,d,w2,a,b,n);
		}
		else if (aux == 3){
			w1 = aw + phi*lw;
			w2 = bw - phi*lw;
			fw1 = f_direction(x,d,w1,a,b,n);
			fw2 = f_direction(x,d,w2,a,b,n);
		}
		if (fw1 < fw2){
			aw = w2;
			aux = 1;
		}
		else{
			bw = w1;
			aux = 2;
		}
		lw = bw-aw;
		lopt = (((b-a)*bw + a)+((b-a)*aw + a))/2.0;
	}
	return lopt;
}

template<typename T>
bool independent(T A){
	T temp = A.transpose();
	A = temp;
	float det = A.determinant();
	if (det != 0){
		return true;
	}
	return false;
}

template<typename vector_n, int n>
vector_n powell(const vector_n & x0, const float & epsilon){
	vector_n x = x0;
	Matrix<float,n,n> d = Matrix<float,n,n>::Identity();
	float a = -10, b = 10;
	while (independent(d)){
		vector_n direction = d.row(n-1);
		float lopt = seccion_dorada(x,direction,a,b,epsilon,n);
		x = x + direction*lopt;
		Matrix<float,n+1,n> y;
		y.row(0) = x;
		for (int iter = 0; iter < n; iter++){
			direction = d.row(iter);
			lopt = seccion_dorada(x,direction,a,b,epsilon,n);
			x = x + direction*lopt;
			y.row(iter+1) = x;
		}
		vector_n newd= y.row(n) - y.row(0);
		float norm = newd.squaredNorm();
		
		for (int iter = 1; iter < n; iter++){
			d.row(iter) = d.row(iter-1);
		}
		if (norm < epsilon*epsilon){
			break;
		}
		d.row(n-1) = newd/norm;
	}
	return x;
}

int main(){
	Matrix<float,1,2> mat;
	mat << 0.0f,0.0f;
	Matrix<float,1,2> answer = powell<Matrix<float,1,2>,2>(mat,0.001f);
	for (int x = 0; x < 2; x++){
		cout << answer(0,x) << " ";
	}
	cout << endl;
	cout << f(answer) << endl;
	
	cout << 8*answer(0)-4*answer(1)+1 << " ";
	cout << 6*answer(1)-4*answer(0) << endl;
	return 0;
}
