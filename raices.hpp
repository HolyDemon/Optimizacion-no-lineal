#pragma once
#include "raices.cpp"

using namespace std;
namespace raices{

float newton_raphson(float & x, const float & eps);
float derivada_en_x(const float & x, const float & delta);
float derivada_2_en_x(const float & x, const float & delta);
float newton_raphson(float & x, const float & delta, const float & eps);
}
