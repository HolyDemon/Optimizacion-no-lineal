#include <iostream>
#include <cstdlib>
#include <ctime>
#include <cmath>
#include <vector>
#include <eigen3/Eigen/Dense>
#include "function.hpp"

using namespace std;
using namespace Eigen;

Function f;
dFunction df;

template<typename vector_n>
float f_direction(vector_n x, vector_n d, const float & w, const float & a, const float & b, const int & n){
	float l = (b - a)*w + a;
	vector<float> x_vector;
	for (int iter = 0; iter < n; iter++){
		x_vector.push_back(x(iter) + l*d(iter));
	}
	return f(x_vector);
}

template<typename vector_n>
float seccion_dorada(vector_n x, vector_n d, const float & a, const float & b, const float & epsilon, const int & n){
	float phi = 0.618;
	float aw = 0, bw = 1, lw = 1;
	int aux = 3;
	float lopt = 0;
	float w1,w2,fw1,fw2;
	while (lw > epsilon/(b-a)){
		if (aux == 1){
			w2 = w1;
			fw2 = fw1;
			w1 = aw + phi*lw;
			fw1 = f_direction(x,d,w1,a,b,n);
		}
		else if (aux == 2){
			w1 = w2;
			fw1 = fw2;
			w2 = bw - phi*lw;
			fw2 = f_direction(x,d,w2,a,b,n);
		}
		else if (aux == 3){
			w1 = aw + phi*lw;
			w2 = bw - phi*lw;
			fw1 = f_direction(x,d,w1,a,b,n);
			fw2 = f_direction(x,d,w2,a,b,n);
		}
		if (fw1 < fw2){
			aw = w2;
			aux = 1;
		}
		else{
			bw = w1;
			aux = 2;
		}
		lw = bw-aw;
		lopt = (((b-a)*bw + a)+((b-a)*aw + a))/2.0;
	}
	return lopt;
}

template<typename T>
bool independent(T A){
	T temp = A.transpose();
	A = temp;
	float det = A.determinant();
	if (det != 0){
		return true;
	}
	return false;
}

template<typename vector_n, int n>
vector_n fletcher(const vector_n & x0, const float & epsilon1, const float & epsilon2, const float & epsilon3){
	vector<vector_n> x;
	vector_n new_gradient,gradient,s;
	
	x.push_back(x0);
	gradient = df(x0);
	s = - 1.0f*gradient;
	float l = seccion_dorada(x[0],s,- 10.0f, 10.0f, epsilon1,n);
	x.push_back(x[0]+l*s);
	new_gradient = df(x[1]);

	while (true){
		float new_norm = new_gradient.squaredNorm(), norm = gradient.squaredNorm();
		s = - 1.0f*new_gradient + ((new_norm)/(norm))*s;
		l = seccion_dorada(x[x.size()-1],s,- 10.0f, 10.0f, epsilon1,n);
		x.push_back(x[x.size()-1] + l*s);
		
		if ((x[x.size()-1]-x[x.size()-2]).squaredNorm()/(x[x.size()-2]).squaredNorm() < epsilon2){
			break;
		}
		gradient = new_gradient;
		new_gradient = df(x[x.size()-1]);
		if (new_gradient.squaredNorm() < epsilon3*epsilon3){
			break;
		}
	}
	return x[x.size()-1];
}

int main(){
	Matrix<float,1,2> mat;
	mat << 0.0f,0.0f;
	Matrix<float,1,2> answer = fletcher<Matrix<float,1,2>,2>(mat,0.05f,0.05f,0.05f);
	
	for (int x = 0; x < 2; x++){
		cout << answer(0,x) << " ";
	}
	cout << endl;
	cout << 1 + 4*answer(0) + 2*answer(1) << " ";
	cout << - 1 + 2*answer(0) + 2*answer(1) << endl;
	return 0;
}
