#include <iostream>
#include "function.hpp"
#include "util.hpp"
#include <cmath>
#include <vector>

using namespace std;
using interval = vector<float>;

struct comparasion{
  bool operator() (const float & a, const float & b){
    return (f(a) < f(b));
  }
};

comparasion comp;
namespace aprx{

float min_cuadr(const float & x1, const float & x2, const float & x3){
  float f1 = f(x1);
  float f2 = f(x2);
  float f3 = f(x3);
  float a1 = f1;
  float a2 = (f2-f1)/(x2-x1);
  float a3 = (1/(x3-x2))*(((f3-f1)/(x3-x1))-((f2-f1)/(x2-x1)));
  float minimo_x = (((x1+x2)/2) - (a2/(2*a3)));
  if (a3 > 0){
    return minimo_x;
  }
  float no_minimo;
  return no_minimo;
}

float min_cubic(const float & x1, const float & x2){
  float f1 = f(x1);
  float f2 = f(x2);
  float df1 = df(x1);
  float df2 = df(x2);
  float z = 3*(f1*f2)/(x2-x1) + df1 + df2;
  float w;
  if (x1 < x2){
    w = sqrt(z*z - df1*df2);
  }
  else{
    w = -sqrt(z*z - df1*df2);
  }
  float mau = (df2+w-z)/(df2-df1+2*w);
  if (mau < 0){
    return x2;
  }
  else if (mau <= 1){
    return x2-mau*(x2-x1);
  }
  return x1;
}

float aprox_cuadratica(const interval & intervalo){
  float x1 = intervalo[0];
  float x3 = intervalo[1];
  float x2 = (x1+x3)/2;
  float respuesta = min_cuadr(x1,x2,x3);
  return respuesta;

}

float cuadr_sucesivas(float & x1,  const float & delta, const float & eps){
  float x2 = x1 + delta;
  float x3;
  if (f(x1+delta) < f(x1)){
    x3 = x1 + 2*delta;
  }
  else{
    x3 = x1 - delta;
  }
  ordenar(x1,x2,x3);
  float xb = min_cuadr(x1,x2,x3);
  float x_min = min(x1,min(x2,x3,comp),comp);
  while (abs(x_min-xb) > eps){
    top_tres(xb,x1,x2,x3);
    ordenar(x1,x2,x3);
    xb = min_cuadr(x1,x2,x3);
    x_min = min(x1,min(x2,x3,comp),comp);
  }
  return min_cuadr(x1,x2,x3);
}

float biseccion(const interval & intervalo, const float & eps){
  float a = intervalo[0];
  float b = intervalo[1];
  float z = (a+b)/2;
  while (abs(df(z)) > eps){
    if (df(z) < 0){
      a = z;
    }
    else{
      b = z;
    }
    z = (a+b)/2;
  }
  return z;
}

float secante(const interval & intervalo, const float & eps){
  float xl = intervalo[0];
  float xr = intervalo[1];
  float z = xr - (df(xr)*(xr-xl))/(df(xr)-df(xl));
  while (abs(df(z)) > 0){
    if (df(z) < 0){
      xl = z;
    }
    else{
      xr = z;
    }
    z = xr - (df(xr)*(xr-xl))/(df(xr)-df(xl));
  }
  return z;
}

float aprox_cubica(float & x, float & delta, const float & eps1, const float & eps2){
  int k = 0;
  if (df(x) > 0){
    delta = -delta;
  }
  float xnew = x+(1<<k)*delta;
  while (df(x)*df(xnew) > 0){
    k++;
    x = xnew;
    xnew = x+(1<<k)*delta;
  }
  float x_min;
  while (true){
    x_min = min_cubic(x,xnew);
    if (abs((x_min - x)/x_min) <= eps2){
      return x_min;
    }
    while (f(x_min) >= f(x)){
      x_min = x_min-0.5*(x_min-x);
    }
    if (df(x_min) <= eps1){
      return x_min;
    }
    if (df(x_min)*df(x) < 0){
      xnew = x_min;
    }
    else{
      x = x_min;
    }
  }
  return x_min;
}

}
