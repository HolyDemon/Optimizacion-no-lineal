#pragma once
#include "busqueda.cpp"
#include "function.hpp"
namespace busque{

vector<float> calculate_fibonacci(const int & n);

interval busqueda_fibonacci(const interval & intervalo, const int & n);

interval seccion_dorada(const interval & intervalo, const float & eps);
}
